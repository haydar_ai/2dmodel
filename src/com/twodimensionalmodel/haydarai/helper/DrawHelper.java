package com.twodimensionalmodel.haydarai.helper;

import java.awt.Graphics;
import java.awt.Point;

import com.twodimensionalmodel.haydarai.model.TwoDimensional;

public class DrawHelper {
	public static void drawPoints(Graphics g, TwoDimensional model, Point[] points) {
		for (int i = 0; i < model.getPoint(); i++) {
			g.drawOval(points[i].x, points[i].y, 1, 1);
		}
	}

	public static void drawLines(Graphics g, TwoDimensional model, Point[] points) {
		for (int i = 0; i < model.getLine(); i++) {
			g.drawLine((int) points[model.getLinesX()[i]].getX(),
					(int) points[model.getLinesX()[i]].getY(),
					(int) points[model.getLinesY()[i]].getX(),
					(int) points[model.getLinesY()[i]].getY());
		}
	}
}
