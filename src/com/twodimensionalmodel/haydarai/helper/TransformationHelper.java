package com.twodimensionalmodel.haydarai.helper;

import java.awt.Point;

public class TransformationHelper {

	public static Point[] translate(Point[] points, int x, int y) {
		for (int i = 0; i < points.length; i++) {
			points[i].translate(x, y);
		}
		return points;
	}

	public static Point[] scale(Point[] points, double scale) {
		for (int i = 0; i < points.length; i++) {
			points[i].x *= scale;
			points[i].y *= scale;
		}
		return points;
	}

	public static Point[] rotate(Point[] points, int degree, int centerX,
			int centerY) {
		double angle = (degree % 360) * (Math.PI / 180);
		Point center = new Point(centerX, centerY);
		for (int i = 0; i < points.length; i++) {
			points[i].x = (int) (Math.cos(angle) * (points[i].x - center.x)
					- Math.sin(angle) * (points[i].y - center.y) + center.x);
			points[i].y = (int) (Math.sin(angle) * (points[i].x - center.x)
					+ Math.cos(angle) * (points[i].y - center.y) + center.y);

		}
		return points;
	}
}
