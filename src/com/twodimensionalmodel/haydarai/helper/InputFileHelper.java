package com.twodimensionalmodel.haydarai.helper;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.twodimensionalmodel.haydarai.model.TwoDimensional;

public class InputFileHelper {

	private static BufferedReader br;
	private static String name;
	private static Point[] points;
	private static int[] linesX;
	private static int[] linesY;

	public static TwoDimensional read(String fileName) throws IOException {
		br = new BufferedReader(new FileReader(fileName));
		name = br.readLine();
		int point = 0;
		int line = 0;
		String[] pointandline = br.readLine().split(" ");
		point = Integer.parseInt(pointandline[0]);
		line = Integer.parseInt(pointandline[1]);
		points = new Point[point];
		for (int i = 0; i < point; i++) {
			String[] XandY = br.readLine().split(" ");
			points[i] = new Point(Integer.parseInt(XandY[0]), Integer.parseInt(XandY[1]));
		}
		linesX = new int[line];
		linesY = new int[line];
		for (int i = 0; i < line; i++) {
			String[] lines = br.readLine().split(" ");
			linesX[i] = normalizeIndex(Integer.parseInt(lines[0]));
			linesY[i] = normalizeIndex(Integer.parseInt(lines[1]));
		}
		br.close();
		TwoDimensional model = new TwoDimensional(name, point, line, points, linesX, linesY);
		return model;
	}

	private static int normalizeIndex(int index) {
		return index - 1;
	}
}
