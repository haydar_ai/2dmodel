package com.twodimensionalmodel.haydarai.model;

import java.awt.Point;

public class TwoDimensional {
	private String name;
	private int point;
	private int line;

	public TwoDimensional(String name, int point, int line, Point[] points,
			int[] linesX, int[] linesY) {
		super();
		this.name = name;
		this.point = point;
		this.line = line;
		this.points = points;
		this.linesX = linesX;
		this.linesY = linesY;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public Point[] getPoints() {
		return points;
	}

	public void setPoints(Point[] points) {
		this.points = points;
	}

	public int[] getLinesX() {
		return linesX;
	}

	public void setLinesX(int[] linesX) {
		this.linesX = linesX;
	}

	public int[] getLinesY() {
		return linesY;
	}

	public void setLinesY(int[] linesY) {
		this.linesY = linesY;
	}

	private Point[] points;
	private int[] linesX;
	private int[] linesY;
}