package com.twodimensionalmodel.haydarai;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import com.twodimensionalmodel.haydarai.helper.DrawHelper;
import com.twodimensionalmodel.haydarai.helper.InputFileHelper;
import com.twodimensionalmodel.haydarai.helper.TransformationHelper;
import com.twodimensionalmodel.haydarai.model.TwoDimensional;

public class Main extends Applet implements ActionListener {

	private static TwoDimensional model;
	private static Point[] points;
	private static final String fileName = "D:/Data/Eclipse/2DModel/bangun3d.txt";
	private static Label label;
	private static Label lTranslate1;
	private static Label lTranslate2;
	private static Label lRotate1;
	private static Label lRotate2;
	private static Label lRotate3;
	private static Label lScale;
	private static TextField tfTranslate1;
	private static TextField tfTranslate2;
	private static TextField tfRotate1;
	private static TextField tfRotate2;
	private static TextField tfRotate3;
	private static TextField tfScale;
	private static Button bTranslate;
	private static Button bRotate;
	private static Button bScale;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void init() {
		setInterface();
		setSize(960, 600);
	}

	private void setInterface() {
		try {
			model = InputFileHelper.read(fileName);
			label = new Label(model.getName());
			points = model.getPoints();
			lTranslate1 = new Label("X");
			tfTranslate1 = new TextField("0");
			lTranslate2 = new Label("Y");
			tfTranslate2 = new TextField("0");
			bTranslate = new Button("Translate");
			lRotate1 = new Label("Center X");
			tfRotate1 = new TextField("0");
			lRotate2 = new Label("Center Y");
			tfRotate2 = new TextField("0");
			lRotate3 = new Label("Degree");
			tfRotate3 = new TextField("180");
			bRotate = new Button("Rotate");
			lScale = new Label("Scale");
			tfScale = new TextField("0");
			bScale = new Button("Scale");
			add(label);
			add(lTranslate1);
			add(tfTranslate1);
			add(lTranslate2);
			add(tfTranslate2);
			add(bTranslate);
			add(lRotate1);
			add(tfRotate1);
			add(lRotate2);
			add(tfRotate2);
			add(lRotate3);
			add(tfRotate3);
			add(bRotate);
			add(lScale);
			add(tfScale);
			add(bScale);
			bTranslate.addActionListener(this);
			bRotate.addActionListener(this);
			bScale.addActionListener(this);
		} catch (IOException e) {
			System.out.println("The input file is not well-formed");
			e.printStackTrace();
		}
	}

	public void paint(Graphics g) {
		DrawHelper.drawPoints(g, model, points);
		DrawHelper.drawLines(g, model, points);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bTranslate) {
			TransformationHelper.translate(points,
					Integer.parseInt(tfTranslate1.getText()),
					Integer.parseInt(tfTranslate2.getText()));
			repaint();
		} else if (e.getSource() == bRotate) {
			TransformationHelper.rotate(points,
					Integer.parseInt(tfRotate3.getText()),
					Integer.parseInt(tfRotate1.getText()),
					Integer.parseInt(tfRotate2.getText()));
			repaint();
		} else if (e.getSource() == bScale) {
			TransformationHelper.scale(points,
					Double.parseDouble(tfScale.getText()));
			repaint();
		}
	}
}
